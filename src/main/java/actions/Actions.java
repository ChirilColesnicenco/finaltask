package actions;

import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.assertthat.selenium_shutterbug.utils.web.ScrollStrategy;
import dataprovider.ConfigReader;
import io.cucumber.core.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Actions {

    public static WebDriver driver;
    private static Logger logger = Logger.getLogger(Actions.class.getName());
    private static WebDriverWait wait;

    static {
        try {
            FileHandler fh = new FileHandler("src\\test\\resources\\TestLogs.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Actions(WebDriver driver) {
        Actions.driver = driver;
        wait = new WebDriverWait(driver, ConfigReader.getTimeout());
    }

    public static void waitUntilVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOfAllElements(element));
    }

    public static void click(WebElement element) {
        highLightElement(element);
        waitUntilVisible(element);
        logger.info("Clicked " + element.getText());
        element.click();
    }

    public static void sendKeys(WebElement element, String string) {
        highLightElement(element);
        waitUntilVisible(element);
        logger.info("Text inserted: " + string);
        element.sendKeys(string);
    }

    protected static void acceptPopUp() {
        wait.until(ExpectedConditions.alertIsPresent());
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    protected static void dismissPopUp() {
        wait.until(ExpectedConditions.alertIsPresent());
        Alert alert = driver.switchTo().alert();
        alert.dismiss();
    }

    public static boolean isDisplayed(WebElement element) {
        waitUntilVisible(element);
        highLightElement(element);
        logger.info("is Displayed " + element.getText());
        return element.isDisplayed();
    }

    public static void select(WebElement element, String option) {
        Select select = new Select(element);
        select.selectByVisibleText(option);
    }

    public static void clear(WebElement element) {
        element.clear();
    }

    public static void captureScreen(Scenario scenario) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        BufferedImage image = Shutterbug.shootPage(driver, ScrollStrategy.WHOLE_PAGE, true).getImage();
        ImageIO.write(image, "png", output);
        final byte[] screenShot = output.toByteArray();
        scenario.embed(screenShot, "image/png");
    }

    public static void highLightElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        waitUntilVisible(element);
        js.executeScript("arguments[0].setAttribute('style','background: yellow; border: 2px solid red;');", element);
    }

    public static void cleanScreenshotDirectory() throws IOException {
        ConfigReader configReader = new ConfigReader();
        File prettyCucumber = new File(configReader.getPrettyCucumberPath());
        File htmlReport = new File(configReader.getHtmlReport());
        FileUtils.cleanDirectory(prettyCucumber);
        File[] files = htmlReport.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.getName().endsWith(".png")) {
                    file.delete();
                }
            }
        }
    }
}