package webdriverfactory;

import dataprovider.ConfigReader;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import java.util.HashMap;
import java.util.Map;

public class BrowsersFactory {
    private static Map<String, WebDriver> drivers;

    public static WebDriver getDriver() {
        return getDriver(System.getProperty("browser"));
    }

    public static WebDriver getDriver(String browserName) {
        drivers = new HashMap<>();
        ConfigReader configReader = new ConfigReader();
        WebDriver driver = null;
        switch (browserName) {
            case "Chrome":
                driver = drivers.get("Chrome");
                if (driver == null) {
                    System.setProperty("webdriver.chrome.driver", ConfigReader.getDriverPath("Chrome"));
                    driver = new ChromeDriver();
                    drivers.put("Chrome", driver);
                }
                break;
            case "InternetExplorer":
                InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions()
                        .introduceFlakinessByIgnoringSecurityDomains()
                        .disableNativeEvents()
                        .ignoreZoomSettings()
                        .enablePersistentHovering()
                        .destructivelyEnsureCleanSession()
                        .setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
                driver = drivers.get("InternetExplorer");
                if (driver == null) {
                    System.setProperty("webdriver.ie.driver", ConfigReader.getDriverPath("InternetExplorer"));
                    driver = new InternetExplorerDriver(internetExplorerOptions);
                    drivers.put("InternetExplorer", driver);
                }
                break;
        }
        return driver;
    }

    public static void closeAllDriver() {
        for (String key : drivers.keySet()) {
            drivers.get(key).close();
            drivers.get(key).quit();
        }
        drivers = null;
    }
}