package dataprovider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

    private static final String propertiesFilePath = ".\\src\\test\\resources\\configuration.properties";
    private static Properties properties;

    public ConfigReader() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertiesFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String getDriverPath(String browserName) {
        String path = null;
        switch (browserName) {
            case "Chrome":
                path = properties.getProperty("chromeDriverPath");
                break;
            case "InternetExplorer":
                path = properties.getProperty("explorerDriverPath");
                break;
        }
        if (path != null) {
            return path;
        } else throw new RuntimeException("path to driver is not specified");
    }

    public static String getBaseURL() {
        return properties.getProperty("baseURL");
    }

    public static int getTimeout() {
        return Integer.parseInt(properties.getProperty("timeoutForElementVisibility"));
    }

    public static String getBrowserName() {
        return properties.getProperty("browser");
    }

    public String getPrettyCucumberPath() {
        return properties.getProperty("prettyCucumberPath");
    }

    public String getHtmlReport() {
        return properties.getProperty("htmlReport");
    }
}