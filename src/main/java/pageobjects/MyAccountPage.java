package pageobjects;

import actions.Actions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage extends Actions {

    @FindBy(xpath = "//span[contains(text(),'Order history and details')]")
    private WebElement orderHistoryButton;
    @FindBy(xpath = "//span[contains(text(),'My addresses')]")
    private WebElement myAddressButton;
    @FindBy(xpath = "//span[contains(text(),'My wishlists')]")
    private WebElement myWishListsButton;

    public MyAccountPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getOrderHistoryButton() {
        return orderHistoryButton;
    }

    public WebElement getMyAddressButton() {
        return myAddressButton;
    }

    public WebElement getMyWishListsButton() {
        return myWishListsButton;
    }
}