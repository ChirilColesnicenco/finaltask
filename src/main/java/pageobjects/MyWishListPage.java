package pageobjects;

import actions.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class MyWishListPage extends Actions {
    @FindBy(xpath = "//table[@class='table table-bordered']//tbody")
    private WebElement tableOfWishList;
    @FindBy(linkText = "My wishlist")
    private WebElement wishList;
    @FindBy(xpath = "//*[contains(@id,'wishlist_')]/td[6]/a")
    private WebElement removeWishListButton;
    @FindBy(xpath = "//*[contains(@id,'_1')]/div/div[2]/div/a")
    private WebElement removeProduct1FromWishList;
    @FindBy(xpath = "//*[contains(@id,'_2')]/div/div[2]/div/a")
    private WebElement removeProduct2FromWishList;
    @FindBy(xpath = "//*[@id=\"priority_1_0\"]")
    private WebElement priority;
    @FindBy(xpath = "//*[@id=\"wlp_1_0\"]/div/div[2]/div/div[2]/a/span")
    private WebElement saveButton;
    @FindBy(xpath = "//*[@id=\"mywishlist\"]/ul/li[1]/a/span")
    private WebElement backToYourAccountButton;
    @FindBy(xpath = "//*[@id=\"mywishlist\"]/ul/li[2]/a/span")
    private WebElement homeButton;
    @FindBy(xpath = "//*[@id=\"name\"]")
    private WebElement newWishListField;
    @FindBy(xpath = "//*[@id=\"submitWishlist\"]/span")
    private WebElement saveNewWishListButton;

    public MyWishListPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public boolean findWishList(String nameWishList) {
        List<WebElement> rows = tableOfWishList.findElements(By.tagName("tr"));
        for (int i = 1; i <= rows.size(); i++) {
            String name = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/table[1]/tbody[1]/tr[" + i + "]/td[1]"))
                    .getText();
            if (name.equals(nameWishList)) {
                return true;
            }
        }
        return false;
    }

    public void removeAllWishLists() throws InterruptedException {
        List<WebElement> rows = tableOfWishList.findElements(By.tagName("tr"));
        for (int i = 1; i <= rows.size(); i++) {
            click(removeWishListButton);
            acceptPopUp();
            Thread.sleep(2000);
        }
    }

    public String prioritySelected() {
        waitUntilVisible(priority);
        Select select = new Select(priority);
        return select.getFirstSelectedOption().getText().replaceAll("\\s+", "");
    }

    public WebElement getRemoveProduct2FromWishList() {
        return removeProduct2FromWishList;
    }

    public WebElement getTableOfWishList() {
        return tableOfWishList;
    }

    public WebElement getWishList() {
        return wishList;
    }

    public WebElement getRemoveWishListButton() {
        return removeWishListButton;
    }

    public WebElement getRemoveProduct1FromWishList() {
        return removeProduct1FromWishList;
    }

    public WebElement getPriority() {
        return priority;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public WebElement getBackToYourAccountButton() {
        return backToYourAccountButton;
    }

    public WebElement getHomeButton() {
        return homeButton;
    }

    public WebElement getNewWishListField() {
        return newWishListField;
    }

    public WebElement getSaveNewWishListButton() {
        return saveNewWishListButton;
    }
}
