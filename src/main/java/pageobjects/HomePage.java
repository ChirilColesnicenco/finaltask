package pageobjects;

import actions.Actions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HomePage extends Actions {

    @FindBy(xpath = "//*[@id=\"search_query_top\"]")
    private WebElement searchField;
    @FindBy(xpath = "//*[@id=\"searchbox\"]/button")
    private WebElement searchButton;
    @FindBy(xpath = "//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")
    private WebElement signInButton;
    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[1]/a")
    private WebElement womenCategory;
    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[2]/a")
    private WebElement dressCategory;
    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[3]/a")
    private WebElement tshirtCategory;
    @FindBy(xpath = "//a[@class='logout']")
    private WebElement logOutButton;
    @FindBy(xpath = "//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")
    private WebElement userAccountButton;
    @FindBy(xpath = "//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a")
    private WebElement cartButton;


    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getCartButton() {
        return cartButton;
    }

    public WebElement getUserAccountButton() {
        return userAccountButton;
    }

    public WebElement getLogOutButton() {
        return logOutButton;
    }

    public WebElement getSearchField() {
        return searchField;
    }

    public WebElement getSearchButton() {
        return searchButton;
    }

    public WebElement getSignInButton() {
        return signInButton;
    }

    public WebElement getWomenCategory() {
        return womenCategory;
    }

    public WebElement getDressCategory() {
        return dressCategory;
    }

    public WebElement getTshirtCategory() {
        return tshirtCategory;
    }
}