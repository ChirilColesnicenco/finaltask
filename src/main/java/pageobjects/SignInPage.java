package pageobjects;

import actions.Actions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInPage extends Actions {

    @FindBy(xpath = "//*[@id=\"email_create\"]")
    private WebElement createAccountEmailAddressField;
    @FindBy(xpath = "//*[@id=\"SubmitCreate\"]/span")
    private WebElement registerButton;
    @FindBy(xpath = "//*[@id=\"email\"]")
    private WebElement loginEmailAddressField;
    @FindBy(xpath = "//*[@id=\"passwd\"]")
    private WebElement loginPasswordField;
    @FindBy(xpath = "//*[@id=\"SubmitLogin\"]/span")
    private WebElement loginButton;

    public SignInPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getCreateAccountEmailAddressField() {
        return createAccountEmailAddressField;
    }

    public WebElement getRegisterButton() {
        return registerButton;
    }

    public WebElement getLoginEmailAddressField() {
        return loginEmailAddressField;
    }

    public WebElement getLoginPasswordField() {
        return loginPasswordField;
    }

    public WebElement getLoginButton() {
        return loginButton;
    }
}