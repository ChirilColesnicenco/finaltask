package pageobjects;

import actions.Actions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BlouseProductPage extends Actions {

    @FindBy(xpath = "//*[@id=\"add_to_cart\"]/button/span")
    private WebElement addToCartButton;
    @FindBy(xpath = "//*[@id=\"quantity_wanted_p\"]/a[2]/span")
    private WebElement quantityAdd;
    @FindBy(xpath = "//*[@id=\"quantity_wanted_p\"]/a[1]/span/i")
    private WebElement quantitySubtract;
    @FindBy(xpath = "//*[@id=\"color_8\"]")
    private WebElement whiteColor;
    @FindBy(xpath = "//*[@id=\"color_11\"]")
    private WebElement greyColor;
    @FindBy(linkText = "Blouse")
    private WebElement productNameInCart;
    @FindBy(linkText = "Color : White, Size : S")
    private WebElement productSpecificationsInCart;
    @FindBy(xpath = "//input[@type='hidden' and @value='2']")
    private WebElement quantityOfProductsInCart;
    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")
    private WebElement closeProductPopUpWindow;

    public BlouseProductPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getAddToCartButton() {
        return addToCartButton;
    }

    public WebElement getProductNameInCart() {
        return productNameInCart;
    }

    public WebElement getProductSpecificationsInCart() {
        return productSpecificationsInCart;
    }

    public WebElement getCloseProductPopUpWindow() {
        return closeProductPopUpWindow;
    }

    public WebElement getQuantityOfProductsInCart() {
        return quantityOfProductsInCart;
    }

    public WebElement getQuantityAdd() {
        return quantityAdd;
    }

    public WebElement getQuantitySubtract() {
        return quantitySubtract;
    }

    public WebElement getWhiteColor() {
        return whiteColor;
    }

    public WebElement getGreyColor() {
        return greyColor;
    }
}
