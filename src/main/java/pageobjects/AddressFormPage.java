package pageobjects;

import actions.Actions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddressFormPage extends Actions {

    @FindBy(xpath = "//input[@id='firstname']")
    private WebElement firstNameTextField;
    @FindBy(xpath = "//input[@id='lastname']")
    private WebElement lastNameTextField;
    @FindBy(xpath = "//input[@id='address1']")
    private WebElement addressTextField;
    @FindBy(xpath = "//input[@id='city']")
    private WebElement cityTextField;
    @FindBy(xpath = "//select[@id='id_state']")
    private WebElement stateDropDownList;
    @FindBy(xpath = "//input[@id='postcode']")
    private WebElement postCodeTextField;
    @FindBy(xpath = "//select[@id='id_country']")
    private WebElement countryDropDownList;
    @FindBy(xpath = "//input[@id='phone']")
    private WebElement homePhoneTextField;
    @FindBy(xpath = "//input[@id='phone_mobile']")
    private WebElement mobilePhoneTextField;
    @FindBy(xpath = "//input[@id='alias']")
    private WebElement addressTitleTextField;
    @FindBy(xpath = "//span[contains(text(),'Save')]")
    private WebElement saveButton;
    @FindBy(xpath = "//span[contains(text(),'Back to your addresses')]")
    private WebElement backToMyAddressesPageButton;
    @FindBy(xpath = "//li[contains(text(),'The alias \"NewTestAddress\" has already been used. Please select another one.')]")
    private WebElement nonUniqueAddressTitleError;

    public AddressFormPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getNonUniqueAddressTitleError() {
        return nonUniqueAddressTitleError;
    }

    public WebElement getFirstNameTextField() {
        return firstNameTextField;
    }

    public WebElement getLastNameTextField() {
        return lastNameTextField;
    }

    public WebElement getAddressTextField() {
        return addressTextField;
    }

    public WebElement getCityTextField() {
        return cityTextField;
    }

    public WebElement getStateDropDownList() {
        return stateDropDownList;
    }

    public WebElement getPostCodeTextField() {
        return postCodeTextField;
    }

    public WebElement getCountryDropDownList() {
        return countryDropDownList;
    }

    public WebElement getHomePhoneTextField() {
        return homePhoneTextField;
    }

    public WebElement getMobilePhoneTextField() {
        return mobilePhoneTextField;
    }

    public WebElement getAddressTitleTextField() {
        return addressTitleTextField;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public WebElement getBackToMyAddressesPageButton() {
        return backToMyAddressesPageButton;
    }
}