package pageobjects;

import actions.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MyAddressesPage extends Actions {

    private static final String myAddressesPageURL = "http://automationpractice.com/index.php?controller=addresses";

    @FindBy(xpath = "//span[contains(text(),'Add a new address')]")
    private WebElement addNewAddressButton;
    @FindBy(xpath = "//div[@class='addresses']")
    private WebElement addressesArea;
    @FindBy(xpath = "//span[contains(text(),'Delete')]")
    private WebElement generalDeleteButton;
    @FindBy(xpath = "//span[contains(text(),'Update')]")
    private WebElement generalUpdateButton;
    @FindBy(xpath = "//p[@class='alert alert-warning']")
    private WebElement noAddressesAreAvailable;

    public MyAddressesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getAddNewAddressButton() {
        return addNewAddressButton;
    }

    public void deleteAllAddressesFromMyAddressesPage() {
        try {
            waitUntilVisible(addressesArea);
            List<WebElement> list = addressesArea.findElements(By.tagName("ul"));
            for (WebElement element : list) {
                click(generalDeleteButton);
                acceptPopUp();
            }
        } catch (TimeoutException e) {
            waitUntilVisible(noAddressesAreAvailable);
        }
    }

    public void deleteAppropriateAddressFromMyAddressesPage(String addressTitle, String approve) {
        waitUntilVisible(addressesArea);
        List<WebElement> list = addressesArea.findElements(By.tagName("ul"));
        for (WebElement element : list) {
            if (element.getText().contains(addressTitle.toUpperCase())) {
                click(element.findElement(By.linkText(generalDeleteButton.getText())));
                if (approve.equals("Approve")) {
                    acceptPopUp();
                } else if (approve.equals("Cancel")) {
                    dismissPopUp();
                }
            }
        }
    }

    public void clickUpdateOfAppropriateAddressFromMyAddressesPage(String addressTitle) {
        waitUntilVisible(addressesArea);
        List<WebElement> list = addressesArea.findElements(By.tagName("ul"));
        for (WebElement element : list) {
            if (element.getText().contains(addressTitle.toUpperCase())) {
                click(element.findElement(By.linkText(generalUpdateButton.getText())));
            }
        }
    }

    public boolean verifyAddressOnAddressesPage(String addressTitle) {
        try {
            waitUntilVisible(addressesArea);
            List<WebElement> list = addressesArea.findElements(By.tagName("ul"));
            for (WebElement element : list) {
                return element.getText().contains(addressTitle.toUpperCase()) ? true : true;
            }
            return false;
        } catch (TimeoutException e) {
            waitUntilVisible(noAddressesAreAvailable);
            return false;
        }
    }

    public List<WebElement> getInformationAboutAppropriateAddressOnAddressesPage(String addressTitle) {
        waitUntilVisible(addressesArea);
        List<WebElement> rows = null;
        List<WebElement> list = addressesArea.findElements(By.tagName("ul"));
        for (WebElement element : list) {
            if (element.getText().contains(addressTitle.toUpperCase())) {
                highLightElement(element);
                rows = element.findElements(By.tagName("li"));
                rows = rows.subList(3, 7);
            }
        }
        return rows;
    }

    public String getMyAddressesPageURL() {
        return myAddressesPageURL;
    }
}