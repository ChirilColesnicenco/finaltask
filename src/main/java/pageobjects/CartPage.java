package pageobjects;

import actions.Actions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartPage extends Actions {

    //Summary part of CartPage

    @FindBy(xpath = "/html/body/div/div[2]/div/div[3]/div/div[2]/table/tbody/tr/td[2]/p/a")
    private WebElement productNameFromCart;
    @FindBy(xpath = "//*[@id=\"center_column\"]/p[2]/a[1]/span")
    private WebElement checkoutButton;
    @FindBy(xpath = "//*[@id=\"cart_quantity_up_1_1_0_248561\"]/span/i")
    private WebElement addQuantityButton;
    @FindBy(xpath = "//*[@id=\"cart_quantity_down_1_1_0_248561\"]/span")
    private WebElement subtractQuantityButton;
    @FindBy(xpath = "//*[@id=\"1_1_0_248561\"]/i")
    private WebElement deleteProductButton;
    @FindBy(xpath = "//*[@id=\"product_1_1_0_248561\"]/td[2]/p/a")
    private WebElement productName;
    @FindBy(xpath = "//*[@id=\"product_1_1_0_248561\"]/td[2]/small[2]/a")
    private WebElement productSpecifications;
    @FindBy(xpath = "//*[@id=\"product_1_1_0_248561\"]/td[3]/span")
    private WebElement productAvailable;
    @FindBy(xpath = "//*[@id=\"product_price_1_1_248561\"]/span")
    private WebElement productUnitPrice;
    @FindBy(xpath = "//*[@id=\"product_1_1_0_248561\"]/td[5]/input[2]")
    private WebElement productQuantity;
    @FindBy(xpath = "//*[@id=\"total_product_price_1_1_248561\"]")
    private WebElement productTotalPrice;
    @FindBy(xpath = "//*[@id=\"total_price_without_tax\"]")
    private WebElement cartTotalPrice;
    @FindBy(xpath = "//*[@id=\"cart_title\"]/span")
    private WebElement headerOfCartQuantityOfProducts;
    @FindBy(xpath = "//*[@id=\"center_column\"]/p[2]/a[2]")
    private WebElement continueShoppingButton;
    //Address part of CartPage
    @FindBy(xpath = "//*[@id=\"center_column\"]/form/p/button/span")
    private WebElement checkoutFromAddressPartButton;
    //Shipping part of CartPage
    @FindBy(xpath = "//*[@id=\"form\"]/div/p[2]/label")
    private WebElement boxToAcceptTermsOfService;
    @FindBy(xpath = "//*[@id=\"form\"]/div/div[2]/div[1]/div/div/table/tbody/tr/td[3]")
    private WebElement deliveryDetails;
    @FindBy(xpath = "//*[@id=\"form\"]/p/button/span")
    private WebElement checkoutFromShippingPartButton;
    //Payment part of CartPage
    @FindBy(xpath = "//*[@id=\"total_product\"]")
    private WebElement productTotalPricePayment;
    @FindBy(xpath = "//*[@id=\"total_shipping\"]")
    private WebElement shippingCost;
    @FindBy(xpath = "//*[@id=\"total_price_container\"]")
    private WebElement totalCost;
    @FindBy(xpath = "//*[@id=\"HOOK_PAYMENT\"]/div[1]/div/p/a")
    private WebElement payByBankMethod;
    @FindBy(xpath = "//*[@id=\"HOOK_PAYMENT\"]/div[2]/div/p/a")
    private WebElement payByCheckMethod;
    @FindBy(xpath = "//*[@id=\"cart_navigation\"]/button/span")
    private WebElement confirmOrderButton;
    //Confirmation part of CartPage
    @FindBy(xpath = "//*[@id=\"center_column\"]/div/p/strong")
    private WebElement confirmationOrderHeader;
    @FindBy(xpath = "//*[@id=\"center_column\"]/p/a")
    private WebElement backToOrdersButton;
    @FindBy(xpath = "//*[@id=\"center_column\"]/div/p/strong")
    private WebElement confirmationOfBankWireMethod;
    @FindBy(xpath = "//*[@id=\"center_column\"]/p[1]")
    private WebElement confirmationOfCheckMethod;

    public CartPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getConfirmationOfBankWireMethod() {
        return confirmationOfBankWireMethod;
    }

    public WebElement getConfirmationOfCheckMethod() {
        return confirmationOfCheckMethod;
    }

    public WebElement getCheckoutButton() {
        return checkoutButton;
    }

    public WebElement getProductNameFromCart() {
        return productNameFromCart;
    }

    public WebElement getAddQuantityButton() {
        return addQuantityButton;
    }

    public WebElement getSubtractQuantityButton() {
        return subtractQuantityButton;
    }

    public WebElement getDeleteProductButton() {
        return deleteProductButton;
    }

    public WebElement getProductName() {
        return productName;
    }

    public WebElement getProductSpecifications() {
        return productSpecifications;
    }

    public WebElement getProductAvailable() {
        return productAvailable;
    }

    public WebElement getProductUnitPrice() {
        return productUnitPrice;
    }

    public WebElement getProductQuantity() {
        return productQuantity;
    }

    public WebElement getProductTotalPrice() {
        return productTotalPrice;
    }

    public WebElement getCartTotalPrice() {
        return cartTotalPrice;
    }

    public WebElement getHeaderOfCartQuantityOfProducts() {
        return headerOfCartQuantityOfProducts;
    }

    public WebElement getContinueShoppingButton() {
        return continueShoppingButton;
    }

    public WebElement getCheckoutFromAddressPartButton() {
        return checkoutFromAddressPartButton;
    }

    public WebElement getBoxToAcceptTermsOfService() {
        return boxToAcceptTermsOfService;
    }

    public WebElement getDeliveryDetails() {
        return deliveryDetails;
    }

    public WebElement getCheckoutFromShippingPartButton() {
        return checkoutFromShippingPartButton;
    }

    public WebElement getProductTotalPricePayment() {
        return productTotalPricePayment;
    }

    public WebElement getShippingCost() {
        return shippingCost;
    }

    public WebElement getTotalCost() {
        return totalCost;
    }

    public WebElement getPayByBankMethod() {
        return payByBankMethod;
    }

    public WebElement getPayByCheckMethod() {
        return payByCheckMethod;
    }

    public WebElement getConfirmOrderButton() {
        return confirmOrderButton;
    }

    public WebElement getConfirmationOrderHeader() {
        return confirmationOrderHeader;
    }

    public WebElement getBackToOrdersButton() {
        return backToOrdersButton;
    }
}