package pageobjects;

import actions.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

public class WomenCategoryPage extends Actions {

    @FindBy(xpath = "//*[@id=\"list\"]/a/i")
    private WebElement listViewButton;
    @FindBy(xpath = "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line']")
    private WebElement frameProduct2;
    @FindBy(xpath = "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line first-item-of-tablet-line first-item-of-mobile-line']")
    private WebElement frameProduct1;
    @FindBy(xpath = "/html/body/div/div[1]/header/div[3]/div/div/div[4]/div[1]/div[2]/div[4]/span/span")
    private WebElement closeAddedProductToCart;
    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")
    private WebElement continueShoppingButton;
    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a/span")
    private WebElement proceedToCheckoutButton;
    @FindBy(xpath = "//a[contains(@class,'_1')]")
    private WebElement addToWishListProduct1;
    @FindBy(xpath = "//a[contains(@class,'_2')]")
    private WebElement addToWishListProduct2;
    @FindBy(xpath = "//*[@id=\"category\"]/div[2]/div/div/div/div/p")
    private WebElement addedToWishListMessage;
    @FindBy(xpath = "//*[@id=\"category\"]/div[2]/div/div/a")
    private WebElement closeAddedProductToWishList;
    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[1]/div/div[1]/div/div[1]/a/i")
    private WebElement productQuickViewButton;
    @FindBy(xpath = "//*[@id=\"add_to_cart\"]/button/span")
    private WebElement productAddToCartQuickView;
    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")
    private WebElement closeQuickView;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[2]/ul[1]/li")
    private List<WebElement> listOfProducts;
    @FindBy(xpath = "/html/body/div/div[1]/header/div[3]/div/div/div[4]/div[1]/div[1]/div[2]/span[1]")
    private WebElement productName;
    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[2]/div/div[2]/h5/a")
    private WebElement productPageButton;

    public WomenCategoryPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void addRandomProduct() {
        org.openqa.selenium.interactions.Actions actions = new org.openqa.selenium.interactions.Actions(driver);
        Random random = new Random(0);
        int r = random.nextInt(listOfProducts.size());
        highLightElement(listOfProducts.get(r));
        actions.moveToElement(listOfProducts.get(r)).click(listOfProducts.get(r)
                .findElement(By.xpath("//*[@id=\"center_column\"]/ul/li[" + (r + 1) + "]/div/div[2]/div[2]/a[1]/span")))
                .build().perform();
    }

    public void addRandomProductQuickView() {
        org.openqa.selenium.interactions.Actions actions = new org.openqa.selenium.interactions.Actions(driver);
        Random random = new Random(0);
        int r = random.nextInt(listOfProducts.size());
        highLightElement(listOfProducts.get(r));
        actions.moveToElement(listOfProducts.get(r)).click(listOfProducts.get(r)
                .findElement(By.xpath("//*[@id=\"center_column\"]/ul/li[" + (r + 1) + "]/div/div[1]/div/a[2]/span")))
                .build().perform();
    }

    public List<WebElement> getListOfProducts() {
        return listOfProducts;
    }

    public WebElement getListViewButton() {
        return listViewButton;
    }

    public WebElement getProductName() {
        return productName;
    }

    public WebElement getCloseQuickView() {
        return closeQuickView;
    }

    public WebElement getProductPageButton() {
        return productPageButton;
    }

    public WebElement getFrameProduct1() {
        return frameProduct1;
    }

    public WebElement getFrameProduct2() {
        return frameProduct2;
    }

    public WebElement getProductAddToCartQuickView() {
        return productAddToCartQuickView;
    }

    public WebElement getProductQuickViewButton() {
        return productQuickViewButton;
    }

    public WebElement getCloseAddedProductToCart() {
        return closeAddedProductToCart;
    }

    public WebElement getContinueShoppingButton() {
        return continueShoppingButton;
    }

    public WebElement getProceedToCheckoutButton() {
        return proceedToCheckoutButton;
    }

    public WebElement getAddToWishListProduct1() {
        return addToWishListProduct1;
    }

    public WebElement getAddToWishListProduct2() {
        return addToWishListProduct2;
    }

    public WebElement getAddedToWishListMessage() {
        return addedToWishListMessage;
    }

    public WebElement getCloseAddedProductToWishList() {
        return closeAddedProductToWishList;
    }
}