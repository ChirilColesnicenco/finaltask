package steps;

import base.BaseUtil;
import dataprovider.ConfigReader;
import io.cucumber.java.en.Given;

import java.util.NoSuchElementException;

import static actions.Actions.click;
import static actions.Actions.sendKeys;

public class CommonSteps extends BaseUtil {

    @Given("user is logged in with email {string} and password {string}")
    public void userIsLoginWithEmailAndPassword(String email, String password) {
        driver.get(ConfigReader.getBaseURL());
        click(homePage.getSignInButton());
        sendKeys(signInPage.getLoginEmailAddressField(), email);
        sendKeys(signInPage.getLoginPasswordField(), password);
        click(signInPage.getLoginButton());
    }

    @Given("user is on {string} page")
    public void userIsOnMyAddressesPage(String pageName) {
        switch (pageName) {
            case "MyAddresses":
                click(myAccountPage.getMyAddressButton());
                break;
            case "Women category":
                click(homePage.getWomenCategory());
                break;
            case "WishList":
                click(homePage.getUserAccountButton());
                click(myAccountPage.getMyWishListsButton());
                break;
            default:
                throw new NoSuchElementException("No such page, please verify page name");
        }
    }
}