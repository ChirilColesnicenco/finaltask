package steps;

import base.BaseUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static actions.Actions.click;
import static actions.Actions.isDisplayed;
import static actions.Actions.waitUntilVisible;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class CheckoutSteps extends BaseUtil {

    @Given("user added product in cart")
    public void addProductInCart() {
        click(homePage.getWomenCategory());
        womenCategoryPage.addRandomProduct();
        waitUntilVisible(womenCategoryPage.getProductName());
        String nameOfProduct = womenCategoryPage.getProductName().getAttribute("innerText");
        click(womenCategoryPage.getCloseAddedProductToCart());
        click(homePage.getCartButton());
        assertThat("Product added in cart: ", cartPage.getProductNameFromCart().getText(), is(nameOfProduct));
        isDisplayed(cartPage.getProductNameFromCart());
    }

    @When("user clicks on [checkout] button")
    public void checkoutCartStep() {
        click(cartPage.getCheckoutButton());
    }

    @When("user clicks on [checkout] button from Address step")
    public void checkoutAddressStep() {
        click(cartPage.getCheckoutFromAddressPartButton());
    }

    @When("user ticks the \"Terms&Conditions\" box")
    public void acceptTermsBox() {
        click(cartPage.getBoxToAcceptTermsOfService());
    }

    @When("clicks on [checkout] button from Shipping step")
    public void checkoutShippingStep() {
        click(cartPage.getCheckoutFromShippingPartButton());
    }

    @When("user clicks on \"Pay by bank wire\" payment method")
    public void selectBankWirePaymentMethod() {
        click(cartPage.getPayByBankMethod());
    }

    @When("user clicks on \"Pay by check\" payment method")
    public void selectCheckPaymentMethod() {
        click(cartPage.getPayByCheckMethod());
    }

    @When("user clicks on [I confirm my order] button")
    public void confirmationOfCheckout() {
        click(cartPage.getConfirmOrderButton());
    }

    @Then("order is placed and summary of the order is displayed")
    public void verifyOrderBankWireMethodIsPlaced() {
        isDisplayed(cartPage.getConfirmationOfBankWireMethod());
    }

    @Then("an informative message \"Your order on My Store is complete.\" is displayed")
    public void verifyOrderCheckMethodIsPlaced() {
        isDisplayed(cartPage.getConfirmationOfCheckMethod());
    }
}
