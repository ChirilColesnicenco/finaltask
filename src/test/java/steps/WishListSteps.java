package steps;

import base.BaseUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.Select;

import java.util.logging.Logger;

import static actions.Actions.click;
import static actions.Actions.isDisplayed;
import static actions.Actions.sendKeys;
import static actions.Actions.waitUntilVisible;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WishListSteps extends BaseUtil {

    private static Logger logger = Logger.getLogger(WishListSteps.class.getName());

    @When("user adds the first product to WishList")
    public void addFirstAProductToWishList() {
        click(womenCategoryPage.getListViewButton());
        click(womenCategoryPage.getAddToWishListProduct1());
    }

    @When("presses the [View my customer account] button")
    public void goToMyAccountPage() {
        click(homePage.getUserAccountButton());
    }

    @Then("product added is displayed")
    public void productAddedIsDisplayed() {
        isDisplayed(myWishListPage.getRemoveProduct1FromWishList());
    }

    @When("adds the second product to WishList")
    public void addAnotherProductToMyWishList() {
        click(womenCategoryPage.getAddToWishListProduct2());
        click(womenCategoryPage.getCloseAddedProductToWishList());
    }

    @Then("added products are displayed")
    public void productsAddedAreDisplayed() {
        isDisplayed(myWishListPage.getRemoveProduct1FromWishList());
        isDisplayed(myWishListPage.getRemoveProduct2FromWishList());
    }

    @When("closes the informative message \"Added to your wishlist.\"")
    public void closeTheInformativeMessageAddedToYourWishlist() {
        click(womenCategoryPage.getCloseAddedProductToWishList());
    }

    @When("user presses [MY WISHLISTS] button from MyAccount page")
    public void pressMYWISHLISTSButtonFromMyAccountPage() {
        click(myAccountPage.getMyWishListsButton());
    }

    @When("access \"My wishlist\" link from My wishlists page")
    public void accessLinkFromMYWISHLISTSPage() {
        try {
            click(myWishListPage.getWishList());
        } catch (Exception exception) {
            click(myWishListPage.getWishList());
        }
    }

    @When("user removes first product added")
    public void userRemovesFirstProductAdded() {
        click(myWishListPage.getRemoveProduct1FromWishList());
    }

    @Then("first product added is not displayed")
    public void firstProductAddedIsNotDisplayed() {
        boolean noProduct = false;
        try {
            isDisplayed(myWishListPage.getRemoveProduct1FromWishList());
            noProduct = true;
        } catch (TimeoutException e) {
            logger.info("First product added is not displayed");
        }
        assertFalse(noProduct);
    }

    @Given("user added a product to WishList")
    public void userAddedAProductToWishList() {
        click(homePage.getWomenCategory());
        addFirstAProductToWishList();
        click(womenCategoryPage.getCloseAddedProductToWishList());
    }

    @Given("user is on WishList page")
    public void goToWishListPage() {
        goToMyAccountPage();
        click(myAccountPage.getMyWishListsButton());
    }

    @When("user changes to {string} priority the product")
    public void changeTheProductPriority(String priority) {
        Select select = new Select(myWishListPage.getPriority());
        select.selectByVisibleText(priority);
    }

    @When("clicks the [Save] button")
    public void clickTheSaveButton() {
        click(myWishListPage.getSaveButton());
    }

    @Then("priority {string} is displayed for the product")
    public void priorityIsDisplayedForTheProduct(String priority) {
        assertEquals(priority, myWishListPage.prioritySelected());
    }

    @When("inserts the name {string} in NEW WISHLISTS field")
    public void insertTheNameInNewWISHLISTSField(String wishListName) {
        sendKeys(myWishListPage.getNewWishListField(), wishListName);
    }

    @When("press the [Save] button")
    public void pressTheSaveButton() {
        click(myWishListPage.getSaveNewWishListButton());
    }

    @Then("wishList {string} appears in a table on the page")
    public void wishlistAppearsInATableOnThePage(String wishListName) {
        assertTrue(myWishListPage.findWishList(wishListName));
    }

    @Given("user created a Wish List {string}")
    public void createAWishList(String wishListName) {
        click(myAccountPage.getMyWishListsButton());
        sendKeys(myWishListPage.getNewWishListField(), wishListName);
        click(myWishListPage.getSaveNewWishListButton());
        assertTrue(myWishListPage.findWishList(wishListName));
    }

    @Then("on My wishlists page there are no wish lists displayed")
    public void onMyWishlistsPageThereAreNoWishListsDisplayed() {
        boolean noWishLists = false;
        try {
            isDisplayed(myWishListPage.getTableOfWishList());
            noWishLists = true;
        } catch (TimeoutException e) {
            logger.info(" All wish lists were deleted");
        }
        assertFalse(noWishLists);
    }

    @When("user removes all wish lists")
    public void removeAllWishLists() throws InterruptedException {
        myWishListPage.removeAllWishLists();
    }

    @Given("user created new wish lists {string} and {string}")
    public void createTwoWishLists(String wishListName1, String wishListName2) {
        createAWishList(wishListName1);
        sendKeys(myWishListPage.getNewWishListField(), wishListName2);
        click(myWishListPage.getSaveNewWishListButton());
        assertTrue(myWishListPage.findWishList(wishListName2));
    }

    @Given("the priority for the product is {string} by default")
    public void priorityByDefault(String priority) {
        assertEquals(priority, myWishListPage.prioritySelected());
    }
}
