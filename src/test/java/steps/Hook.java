package steps;

import base.BaseUtil;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import pageobjects.AddressFormPage;
import pageobjects.BlouseProductPage;
import pageobjects.CartPage;
import pageobjects.HomePage;
import pageobjects.MyAccountPage;
import pageobjects.MyAddressesPage;
import pageobjects.MyWishListPage;
import pageobjects.SignInPage;
import pageobjects.WomenCategoryPage;
import webdriverfactory.BrowsersFactory;

import java.io.IOException;
import java.util.logging.Logger;

import static actions.Actions.captureScreen;
import static actions.Actions.click;

public class Hook extends BaseUtil {

    private static Logger logger = Logger.getLogger(Hook.class.getName());

    @Before
    public void setup() {
        driver = BrowsersFactory.getDriver();
        driver.manage().window().maximize();
        homePage = new HomePage(driver);
        signInPage = new SignInPage(driver);
        myAccountPage = new MyAccountPage(driver);
        myAddressesPage = new MyAddressesPage(driver);
        addressFormPage = new AddressFormPage(driver);
        womenCategoryPage = new WomenCategoryPage(driver);
        myWishListPage = new MyWishListPage(driver);
        cartPage = new CartPage(driver);
        blouseProductPage = new BlouseProductPage(driver);
        actions = new Actions(driver);
    }

    @After(order = 0)
    public void close() {
        click(homePage.getLogOutButton());
        BrowsersFactory.closeAllDriver();
    }

    @After(value = "@WishList", order = 1)
    public void cleanWishLists() throws InterruptedException {
        try {
            myWishListPage.removeAllWishLists();
        } catch (NoSuchElementException e) {
            logger.info(" All wish lists were deleted");
        }
    }

    @After(value = "@MyAddressesTest", order = 1)
    public void testEnd() {
        myAddressesPage.deleteAllAddressesFromMyAddressesPage();
    }

    @AfterStep
    public void screenShot(Scenario scenario) throws IOException, InterruptedException {
        captureScreen(scenario);
        Thread.sleep(1000);
    }
}