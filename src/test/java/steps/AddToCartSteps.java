package steps;

import base.BaseUtil;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;

import static actions.Actions.click;
import static actions.Actions.isDisplayed;
import static actions.Actions.waitUntilVisible;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class AddToCartSteps extends BaseUtil {

    @When("user add to cart a random product")
    public void addToCart() {
        womenCategoryPage.addRandomProduct();
    }

    @Then("this product is displayed in cart")
    public void checkProductIsInCart() {
        waitUntilVisible(womenCategoryPage.getProductName());
        String nameOfProduct = womenCategoryPage.getProductName().getAttribute("innerText");
        waitUntilVisible(womenCategoryPage.getContinueShoppingButton());
        click(womenCategoryPage.getContinueShoppingButton());
        waitUntilVisible(womenCategoryPage.getProductName());
        click(homePage.getCartButton());
        assertThat("Product added in cart: ", cartPage.getProductNameFromCart().getText(), is(nameOfProduct));
        isDisplayed(cartPage.getProductNameFromCart());
    }

    @When("user clicks on Quick View button for a random product")
    public void productQuickView() {
        womenCategoryPage.addRandomProductQuickView();
        waitUntilVisible(driver.findElement(By.xpath("//*[contains(@id,'fancybox-frame')]")));
        driver.switchTo().frame(driver.findElement(By.xpath("//*[contains(@id,'fancybox-frame')]")));
    }

    @When("add product to cart from the Quick View panel")
    public void addToCartFromQuickView() {
        click(womenCategoryPage.getProductAddToCartQuickView());
    }

    @Then("product is displayed in cart")
    public void productFromQuickViewIsInCart() {
        waitUntilVisible(womenCategoryPage.getProductName());
        String nameOfProductQuickView = womenCategoryPage.getProductName().getAttribute("innerText");
        click(womenCategoryPage.getContinueShoppingButton());
        waitUntilVisible(womenCategoryPage.getProductName());
        click(homePage.getCartButton());
        assertThat("Product added in cart: ", cartPage.getProductNameFromCart().getText(), is(nameOfProductQuickView));
        isDisplayed(cartPage.getProductNameFromCart());
    }

    @When("user clicks on Blouse product")
    public void productPage() {
        click(womenCategoryPage.getProductPageButton());
    }

    @When("choose the following product specification: Color - White")
    public void productSpecifications() {
        click(blouseProductPage.getWhiteColor());
    }

    @When("add product to cart from product page")
    public void addProductToCartFromProductPage() {
        click(blouseProductPage.getAddToCartButton());
        waitUntilVisible(blouseProductPage.getCloseProductPopUpWindow());
        click(blouseProductPage.getCloseProductPopUpWindow());
    }

    @Then("this product with selected specification is displayed in cart")
    public void productInCart() {
        waitUntilVisible(homePage.getCartButton());
        click(homePage.getCartButton());
        isDisplayed(blouseProductPage.getProductNameInCart());
        isDisplayed(blouseProductPage.getProductSpecificationsInCart());
    }
}
