package steps;

import base.BaseUtil;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Map;

import static actions.Actions.clear;
import static actions.Actions.click;
import static actions.Actions.select;
import static actions.Actions.sendKeys;
import static actions.Actions.waitUntilVisible;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class MyAddressesSteps extends BaseUtil {

    @When("user clicks on add new address button")
    public void userClickOnAddNewAddressButton() {
        click(myAddressesPage.getAddNewAddressButton());
    }

    @When("mandatory text-fields were filled with the following values:")
    public void userPopulateAddressFormWithValidValues(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        clear(addressFormPage.getFirstNameTextField());
        clear(addressFormPage.getLastNameTextField());
        clear(addressFormPage.getAddressTitleTextField());
        sendKeys(addressFormPage.getFirstNameTextField(), list.get(0).get("First Name"));
        sendKeys(addressFormPage.getLastNameTextField(), list.get(0).get("Last Name"));
        sendKeys(addressFormPage.getAddressTextField(), list.get(0).get("Address"));
        sendKeys(addressFormPage.getCityTextField(), list.get(0).get("City"));
        sendKeys(addressFormPage.getPostCodeTextField(), list.get(0).get("Postal Code"));
        sendKeys(addressFormPage.getHomePhoneTextField(), list.get(0).get("Phone Number"));
        sendKeys(addressFormPage.getAddressTitleTextField(), list.get(0).get("Address Title"));
    }

    @When("mandatory dropdown-lists were selected with following items:")
    public void selectValuesOfMandatoryDropDowns(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        select(addressFormPage.getStateDropDownList(), list.get(0).get("State"));
        select(addressFormPage.getCountryDropDownList(), list.get(0).get("Country"));
    }

    @When("save button is clicked")
    public void saveButtonIsClicked() {
        click(addressFormPage.getSaveButton());
    }

    @Then("user is redirected to MyAddress page")
    public void userIsRedirectedToMyAddressPage() {
        waitUntilVisible(myAddressesPage.getAddNewAddressButton());
        assertThat("User is not on MyAddresses page: ", driver.getCurrentUrl(), is(myAddressesPage.getMyAddressesPageURL()));
    }

    @Then("{string} address is displayed on MyAddresses page")
    public void newAddressIsAddedOnPage(String addressTitle) {
        assertThat("New Created Address isn't on MyAddresses page", myAddressesPage.verifyAddressOnAddressesPage(addressTitle.toUpperCase()), is(true));
    }

    @Given("on MyAddresses page is displayed a address with following Address title: {string}")
    public void verifyAppropriateAddressOnMyAddressesPage(String arg) {
        click(myAddressesPage.getAddNewAddressButton());
        clear(addressFormPage.getAddressTitleTextField());
        sendKeys(addressFormPage.getAddressTextField(), "TestAddress");
        sendKeys(addressFormPage.getCityTextField(), "TestCity");
        sendKeys(addressFormPage.getPostCodeTextField(), "10001");
        sendKeys(addressFormPage.getHomePhoneTextField(), "022220022");
        sendKeys(addressFormPage.getAddressTitleTextField(), arg);
        select(addressFormPage.getStateDropDownList(), "Texas");
        select(addressFormPage.getCountryDropDownList(), "United States");
        click(addressFormPage.getSaveButton());
    }

    @Then("process of creation is failed with following error: {string}")
    public void addressCreationFail(String errorMessage) {
        waitUntilVisible(addressFormPage.getNonUniqueAddressTitleError());
        assertThat("No such error message: ", addressFormPage.getNonUniqueAddressTitleError().getText(), is(errorMessage));
        click(addressFormPage.getBackToMyAddressesPageButton());
    }

    @When("user clicks on delete button of {string} address and then clicks {string} button in the pop-up message")
    public void userDeleteAppropriateAddress(String addressTitle, String approve) {
        myAddressesPage.deleteAppropriateAddressFromMyAddressesPage(addressTitle, approve);
    }

    @Then("{string} is not more displayed on the MyAddresses page")
    public void verifyIfAddressIsNotMoreDisplayed(String addressTitle) {
        assertThat("New Created Address is on MyAddresses page", myAddressesPage.verifyAddressOnAddressesPage(addressTitle.toUpperCase()), is(false));
    }

    @When("user clicks on update button of {string} address")
    public void clickUpdateButton(String addressTitle) {
        myAddressesPage.clickUpdateOfAppropriateAddressFromMyAddressesPage(addressTitle);
    }

    @When("updates values of mandatory fields with following values:")
    public void updateMandatoryFields(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        clear(addressFormPage.getAddressTextField());
        clear(addressFormPage.getCityTextField());
        clear(addressFormPage.getPostCodeTextField());
        clear(addressFormPage.getHomePhoneTextField());
        sendKeys(addressFormPage.getAddressTextField(), list.get(0).get("Address"));
        sendKeys(addressFormPage.getCityTextField(), list.get(0).get("City"));
        select(addressFormPage.getStateDropDownList(), list.get(0).get("State"));
        sendKeys(addressFormPage.getPostCodeTextField(), list.get(0).get("Post Code"));
        sendKeys(addressFormPage.getHomePhoneTextField(), list.get(0).get("Phone Number"));
    }

    @Then("{string} address was updated with following values:")
    public void verifyAddressInfo(String addressTitle, DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);
        List<WebElement> rows = myAddressesPage.getInformationAboutAppropriateAddressOnAddressesPage(addressTitle);
        String cityStatePostCode = list.get(0).get("City") + ", " + list.get(0).get("State") + " " + list.get(0).get("Post Code");
        assertThat("Address is not such expected", list.get(0).get("Address"), is(rows.get(0).getText()));
        assertThat("| City | State | Postal Code | not as expected", cityStatePostCode, is(rows.get(1).getText()));
        assertThat("Phone Number is not such expected", list.get(0).get("Phone Number"), is(rows.get(3).getText()));
    }
}