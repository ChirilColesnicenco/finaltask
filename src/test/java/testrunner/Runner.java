package testrunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.IOException;

import static actions.Actions.cleanScreenshotDirectory;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/features/"},
        plugin = {"pretty", "html:target/report/", "json:target/report.json", "de.monochromata.cucumber.report.PrettyReports:target/pretty-cucumber"},
        glue = "steps",
        tags = "@AllTests")
public class Runner {
    @BeforeClass
    public static void setUp() throws IOException {
        cleanScreenshotDirectory();
    }
}