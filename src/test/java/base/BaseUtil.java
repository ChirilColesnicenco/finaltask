package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import pageobjects.AddressFormPage;
import pageobjects.BlouseProductPage;
import pageobjects.CartPage;
import pageobjects.HomePage;
import pageobjects.MyAccountPage;
import pageobjects.MyAddressesPage;
import pageobjects.MyWishListPage;
import pageobjects.SignInPage;
import pageobjects.WomenCategoryPage;


public class BaseUtil {
    protected static WebDriver driver;
    protected static HomePage homePage;
    protected static SignInPage signInPage;
    protected static MyAccountPage myAccountPage;
    protected static MyAddressesPage myAddressesPage;
    protected static AddressFormPage addressFormPage;
    protected static WomenCategoryPage womenCategoryPage;
    protected static BlouseProductPage blouseProductPage;
    protected static CartPage cartPage;
    protected static MyWishListPage myWishListPage;
    protected static Actions actions;
}