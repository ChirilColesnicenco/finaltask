@AllTests
@Cart
Feature: Add product to cart

  Background:
    Given user is logged in with email "user@moldova.com" and password "qwerty1"
    And user is on "Women category" page

  Scenario: Add a product to cart
    When user add to cart a random product
    Then this product is displayed in cart

  Scenario: Add a product to cart from Quick View feature
    When user clicks on Quick View button for a random product
    And add product to cart from the Quick View panel
    Then product is displayed in cart

  Scenario: Add a product to cart with different specifications
    When user clicks on Blouse product
    And choose the following product specification: Color - White
    And add product to cart from product page
    Then this product with selected specification is displayed in cart