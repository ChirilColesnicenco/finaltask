@AllTests
@Checkout
Feature: Checkout products from cart

  Background:
    Given user is logged in with email "user@moldova.com" and password "qwerty1"
    And user added product in cart

  Scenario: Checkout using "Pay by bank wire" method
    When user clicks on [checkout] button
    And user clicks on [checkout] button from Address step
    And user ticks the "Terms&Conditions" box
    And clicks on [checkout] button from Shipping step
    And user clicks on "Pay by bank wire" payment method
    And user clicks on [I confirm my order] button
    Then order is placed and summary of the order is displayed

  Scenario: Checkout using "Pay by check" method
    When user clicks on [checkout] button
    And user clicks on [checkout] button from Address step
    And user ticks the "Terms&Conditions" box
    And clicks on [checkout] button from Shipping step
    And user clicks on "Pay by check" payment method
    And user clicks on [I confirm my order] button
    Then an informative message "Your order on My Store is complete." is displayed
