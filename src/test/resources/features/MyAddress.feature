@AllTests
@MyAddressesTest
Feature: MyAddresses Functionality

  Background:
    Given user is logged in with email "roensense@gmail.com" and password "12345"
    And user is on "MyAddresses" page

  Scenario: User adds a new address on MyAddresses page
    When user clicks on add new address button
    And mandatory text-fields were filled with the following values:
      | First Name | Last Name | Address            | City     | Postal Code | Phone Number | Address Title |
      | Gordon     | Freeman   | str.Black Messa 11 | Chisinau | 20003       | +37328222111 | TestAddress   |
    And mandatory dropdown-lists were selected with following items:
      | State | Country       |
      | Texas | United States |
    And save button is clicked
    Then user is redirected to MyAddress page
    And "TestAddress" address is displayed on MyAddresses page

  Scenario: User cancels deletion process
    Given on MyAddresses page is displayed a address with following Address title: "CancelDeleteTestAddress"
    When user clicks on delete button of "CancelDeleteTestAddress" address and then clicks 'Cancel' button in the pop-up message
    Then "CancelDeleteTestAddress" address is displayed on MyAddresses page

  Scenario: User deletes an address from MyAddresses page
    Given on MyAddresses page is displayed a address with following Address title: "DeleteTestAddress"
    When user clicks on delete button of "DeleteTestAddress" address and then clicks 'Approve' button in the pop-up message
    Then "DeleteTestAddress" is not more displayed on the MyAddresses page

  Scenario: User updates address information
    Given on MyAddresses page is displayed a address with following Address title: "UpdateTestAddress"
    When user clicks on update button of "UpdateTestAddress" address
    And updates values of mandatory fields with following values:
      | Address       | City | State   | Post Code | Phone Number |
      | str. Brown 12 | Baht | Alabama | 20005     | +37379111222 |
    And save button is clicked
    Then user is redirected to MyAddress page
    And "UpdateTestAddress" address was updated with following values:
      | Address       | City | State   | Post Code | Phone Number |
      | str. Brown 12 | Baht | Alabama | 20005     | +37379111222 |

  Scenario: User adds a new address on MyAddresses page with non-unique address title
    Given on MyAddresses page is displayed a address with following Address title: "NewTestAddress"
    When user clicks on add new address button
    And mandatory text-fields were filled with the following values:
      | First Name | Last Name | Address     | City | Postal Code | Phone Number | Address Title  |
      | John       | Mnemonic  | str.John 22 | John | 20011       | +37378200200 | NewTestAddress |
    And mandatory dropdown-lists were selected with following items:
      | State | Country       |
      | Texas | United States |
    And save button is clicked
    Then process of creation is failed with following error: "The alias \"NewTestAddress\" has already been used. Please select another one."
