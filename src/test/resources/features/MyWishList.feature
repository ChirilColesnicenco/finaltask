@AllTests
@WishList
Feature: My Wish Lists

  As a user,
  I want to be able to add, delete and update products from "My wishlist"
  and create new wish lists

  Background:
    Given user is logged in with email "uusertest786@gmail.com" and password "qweasd123"

  Scenario: Add a product to MyWishList
    Given user is on "Women category" page
    When user adds the first product to WishList
    And closes the informative message "Added to your wishlist."
    And presses the [View my customer account] button
    And user presses [MY WISHLISTS] button from MyAccount page
    And access "My wishlist" link from My wishlists page
    Then product added is displayed

  Scenario: Add two products to MyWishList and remove one
    Given user is on "Women category" page
    When user adds the first product to WishList
    And closes the informative message "Added to your wishlist."
    And adds the second product to WishList
    And presses the [View my customer account] button
    And user presses [MY WISHLISTS] button from MyAccount page
    And access "My wishlist" link from My wishlists page
    Then added products are displayed
    When user removes first product added
    Then first product added is not displayed

  Scenario: Change the priority for product added to wishList
    Given user added a product to WishList
    And user is on "WishList" page
    And access "My wishlist" link from My wishlists page
    And the priority for the product is "Medium" by default
    When user changes to "Low" priority the product
    And clicks the [Save] button
    And access "My wishlist" link from My wishlists page
    Then priority "Low" is displayed for the product

  Scenario: Create a new Wish List
    When user presses [MY WISHLISTS] button from MyAccount page
    And inserts the name "NewList" in NEW WISHLISTS field
    And press the [Save] button
    Then wishList "NewList" appears in a table on the page

  Scenario: Remove all Wish List created
    Given user created new wish lists "FirstList" and "SecondList"
    When user removes all wish lists
    Then on My wishlists page there are no wish lists displayed